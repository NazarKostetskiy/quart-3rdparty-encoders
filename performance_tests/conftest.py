from timeit import default_timer as timer
import pytest
from yarl import URL


@pytest.fixture
def base_url():
    yield URL("http://127.0.0.1:8000")
