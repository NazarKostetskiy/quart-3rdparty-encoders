
import json
from attrs import asdict
from attr import define
import orjson
from pydantic import BaseModel
from cattr.preconf.orjson import make_converter
import pytest


@define(slots=True)
class AttrsResponseModel:
    type: str
    features: list


class DefaultResponseModel(BaseModel):
    type: str
    features: list


class OrjsonResponseModel(BaseModel):
    type: str
    features: list

    class Config:
        json_loads = orjson.loads
        json_dumps = lambda *args, **kwargs: orjson.dumps(
            *args, **kwargs).decode()


data = {"type": "type", "features": [
    {str(value): value}for value in range(5555555)]}


# Just dict json encode
@pytest.mark.parametrize('dumps', [orjson.dumps, json.dumps])  # ~0.28s, ~1.8s
def test_dumps(dumps):
    dumps(data)


# Similar to quart-schema scenary with validation and dumps
# ~9.9s, ~7.3s
@pytest.mark.parametrize('model,dumps', [(DefaultResponseModel, json.dumps), (OrjsonResponseModel, orjson.dumps)])
def test_pydantic_dict_and_dumps(model, dumps):
    dumps(model(**data).dict())


# Dumps imediatelly after initialization
# ~9s ~7.47s
@pytest.mark.parametrize('model', [DefaultResponseModel, OrjsonResponseModel])
def test_pydantic_json(model):
    model(**data).json()


# # Test attrs to json encode
# ~5.84s, 7.22s
@pytest.mark.parametrize('dumps', [orjson.dumps, json.dumps])
def test_attrs_model_dumps(dumps):
    converter = make_converter()
    dumps(converter.unstructure(AttrsResponseModel(**data)))
