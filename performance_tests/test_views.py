import aiohttp
import pytest


@pytest.mark.parametrize('execution_number', range(5))
async def test_default(base_url, execution_number):
    async with aiohttp.ClientSession() as session:
        async with session.get(base_url / "data") as response:
            assert response.status == 200


"""
Tested on json ~180 mb
"""


"""
Results with default encoder
8.94
8.89
8.87
8.66
8.65
8.63
8.56
8.45
8.31
8.29


using pydantic response model for validation:
25.73s
25.27s
24.97s
24.55s
24.37s
24.36s
24.33s
24.27s
24.07s
23.93s

"""


"""
Results for ujson

5.79
5.89
5.79
5.88
5.58
5.88
5.90
5.52
5.67
5.75
"""


"""
Results for orjson

4.91
4.70
4.65
4.57
4.49
4.41
4.36
4.31
4.25
4.19


Using pydantic response model for validation:
23.51
21.15
21.14
20.75
20.72
20.67
20.62
20.59
20.45
20.12
"""
