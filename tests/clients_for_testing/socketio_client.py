import asyncio
import socketio as sio

socketio = sio.AsyncClient(logger=True, engineio_logger=True)


@socketio.event
async def connect():
    print('connection established')
    await socketio.emit("test_req", "asd")


@socketio.on("test_resp")
async def _test_response_handler(msg):
    print(f"Got response on test request with message: {msg}")


@socketio.event
async def disconnect():
    print('disconnected from server')


async def main():
    await socketio.connect("http://localhost:8000")
    await socketio.wait()


if __name__ == '__main__':
    asyncio.run(main())
