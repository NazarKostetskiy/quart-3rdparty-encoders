import sseclient as sseclient


def get_recent_change() -> list:
    msgs = sseclient.SSEClient("http://127.0.0.1:8000/sse", retry=500)
    for msg in msgs:
        print(msg)


if __name__ == '__main__':
    get_recent_change()
