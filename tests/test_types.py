from decimal import Decimal
from uuid import uuid4
import pytest
from datetime import datetime
from app.utils.encoders import OrJsonEncoder, UltraJsonEncoder
import json


datatime_now = datetime.now()
time = datetime.now().time()

decimal_value = Decimal(3.14)
NaN_Decimal = Decimal("NaN")

uuid = uuid4()


@pytest.mark.parametrize("encoder,date", [(UltraJsonEncoder(), datatime_now), (OrJsonEncoder(), datatime_now)])
def test_datetime(encoder, date):
    res = encoder.encode({"date": date})

    json_data = json.loads(res)
    assert isinstance(json_data["date"], str)


@pytest.mark.parametrize("encoder,time", [(UltraJsonEncoder(), time), (OrJsonEncoder(), time)])
def test_time(encoder, time):
    res = encoder.encode({"time": time})

    json_data = json.loads(res)
    assert isinstance(json_data["time"], str)


@pytest.mark.parametrize("encoder,decimal", [(UltraJsonEncoder(), decimal_value), (OrJsonEncoder(), decimal_value),
                                             (UltraJsonEncoder(), NaN_Decimal), (OrJsonEncoder(), NaN_Decimal)])
def test_decimal(encoder, decimal):
    res = encoder.encode({"decimal": decimal})

    json_data = json.loads(res)
    assert isinstance(json_data["decimal"], float)


@pytest.mark.parametrize("encoder,uuid", [(UltraJsonEncoder(), uuid), (OrJsonEncoder(), uuid)])
def test_uuid(encoder, uuid):
    res = encoder.encode({"uuid": uuid})

    json_data = json.loads(res)
    assert isinstance(json_data["uuid"], str)


"""
Result:

Ultrajson has bad default encoder without support of decimal, uuid, datetime and other types.
"""
