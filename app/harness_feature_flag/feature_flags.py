from .constants import DEFAULT_FFS_VALUES

feature_flags = {**DEFAULT_FFS_VALUES}  # Mutable dict
