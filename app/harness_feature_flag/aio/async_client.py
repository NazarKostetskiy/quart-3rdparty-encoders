import asyncio
import json
import logging
import socket
from datetime import timedelta
from typing import Dict

import aiohttp
from aiohttp import ClientConnectionError
from aiohttp_sse_client import client as sse_client

from harness_feature_flag.aio.structures import FeatureFlag, FFBoolState
from harness_feature_flag.aio.utils import decode_jwt
from messaging.data import Data
from messaging.mbox import Mbox
from messaging.messages import MsgID
from messaging.post_office import PostOffice

BASE_URL = "https://config.ff.harness.io/api/1.0"
EVENTS_URL = "https://events.ff.harness.io/api/1.0"

MINUTE = 60
PULL_INTERVAL = 1 * MINUTE
PERSIST_INTERVAL = 1 * MINUTE
EVENTS_SYNC_INTERVAL = 1 * MINUTE

SDK_VERSION = "1.0"

logger = logging.getLogger(__name__)


class AsyncHarnessFeatureFlagsClient:
    def __init__(self, api_key: str, feature_flags: Dict[str, FeatureFlag], base_url=None, events_url=None):
        self._api_key = api_key
        self._auth_token = None

        self._base_url = base_url or BASE_URL
        self._events_url = events_url or EVENTS_URL

        self._environment_id = None
        self._cluster = None

        # Mutable
        self._feature_flags = feature_flags

    @property
    def base_url(self):
        return self._base_url

    @property
    def authorized_headers(self):
        return {"Authorization": f"Bearer {self._auth_token}",
                'API-Key': self._api_key,
                "User-Agent": "PythonSDK/" + SDK_VERSION,
                "Content-Type": "application/json"
                }

    async def authorize(self):
        async with aiohttp.ClientSession() as session:
            # Cannot use there just json keyword: it doesn't like application/json with charset=utf8
            async with session.post(self._base_url + '/client/auth',
                                    data=json.dumps({"apiKey": self._api_key}),
                                    headers={"Content-Type": "application/json"}) as resp:
                res = await resp.json()
                auth_token = res.get("authToken")

                if not auth_token:
                    raise ValueError("Something went wrong on authorization. Cannot get token")

                self._auth_token = auth_token

                decoded = decode_jwt(self._auth_token, options={
                    "verify_signature": False})
                self._environment_id = decoded["environment"]
                self._cluster = decoded["clusterIdentifier"]
                if not self._cluster:
                    self._cluster = '1'

                return auth_token

    async def get_flag_state(self, flag_name: str):
        """Get info about updated flag"""
        url = f"https://config.ff.harness.io/api/1.0/client/env/{self._environment_id}/feature-configs/{flag_name}?cluster={self._cluster}"
        async with aiohttp.ClientSession() as session:
            # Cannot use there just json keyword: it doesn't like application/json with charset=utf8
            async with session.get(url, headers=self.authorized_headers) as resp:
                res = await resp.json()
                return res

    async def _handle_feature_flag_event(self, event):
        data = json.loads(event.data)
        flag_id = data.get("identifier")
        if flag_id:
            flag = await self.get_flag_state(flag_id)
            await self._filter_and_update_flag(flag)

    async def _filter_and_update_flag(self, flag_data: dict):
        """Filter events by project, flag, etc. and update these values in project."""
        try:
            environment = flag_data["environment"]
            project = flag_data["project"]
            flag_name = flag_data["feature"]

            used_feature_flag = self._feature_flags.get(flag_name)
            if used_feature_flag:
                if all([used_feature_flag.environment == environment, used_feature_flag.project == project]):
                    new_value = FFBoolState(flag_data["state"]).to_bool()
                    logger.info(f"Update {flag_name} feature flag state.")
                    used_feature_flag.value = new_value

                    # Send to application queue directly because on multiple workers we will have duplicated messages
                    # if we will use pubsub queue
                    await PostOffice.send_msg(Mbox.UPDATER, Data(MsgID.MSG_FEATURE_FLAG_UPDATE_EVENT, flag_data))
                    return used_feature_flag

            logger.warning(f"Unused feature flag: {flag_name}. Skip...")

        except KeyError as e:
            logger.error(e)
            raise Exception(f"Invalid response: {e}")

    async def run(self):
        while True:
            logger.info("Connecting to harness feature flags stream...")
            try:
                await self.authorize()
                async with sse_client.EventSource(
                        f"https://config.ff.harness.io/api/1.0/stream?cluster={self._cluster}",
                        reconnection_time=timedelta(seconds=3),
                        max_connect_retry=2,
                        headers=self.authorized_headers,
                        timeout=0
                ) as event_source:
                    logger.info("Connected and authorized to harness feature flag stream.")
                    async for event in event_source:
                        logger.info(f"Got Harness FeatureFlag event: {event}")
                        # Handling event in background...
                        asyncio.create_task(self._handle_feature_flag_event(event))

            except (ConnectionError, ClientConnectionError, socket.gaierror) as e:
                logger.error(f"Connection error: {e}\n"
                             f"Reconnecting...")
                pass

            except Exception as e:
                logger.error(f"Unexpected error: {e}")

            await asyncio.sleep(10)
