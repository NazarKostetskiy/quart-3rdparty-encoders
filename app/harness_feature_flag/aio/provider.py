import asyncio

from config import HARNESS_API_KEY
from harness_feature_flag.aio.async_client import AsyncHarnessFeatureFlagsClient
from harness_feature_flag.feature_flags import feature_flags


def initialize_and_run_feature_flags_manager() -> AsyncHarnessFeatureFlagsClient:
    ff_client = AsyncHarnessFeatureFlagsClient(HARNESS_API_KEY, feature_flags=feature_flags)
    asyncio.create_task(ff_client.run())
    return ff_client
