from jwt import decode


def decode_jwt(jwt: str, *args, **kwargs):
    return decode(jwt, *args, **kwargs)
