from dataclasses import dataclass
from enum import Enum


class FFBoolState(str, Enum):
    __slots__ = ()

    ON = "on"
    OFF = "off"

    def to_bool(self):
        if self.value == self.ON:
            return True
        return False

    @classmethod
    def from_bool(cls, value: bool):
        if value:
            return cls(cls.ON)
        return cls(cls.OFF)


@dataclass  # (slots=True)
class FeatureFlag:
    name: str
    identifier: str
    value: bool

    # Metadata
    # TODO: Take it from config
    environment: str = "dev"
    account: str = "qHyY6MHlRuyFvoQnEKiCvA"
    project: str = "Studio_FFs"
    org: str = "default"
