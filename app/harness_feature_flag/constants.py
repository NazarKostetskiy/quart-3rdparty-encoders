from harness_feature_flag.aio.structures import FeatureFlag


PUSHDOWN_FF = "Pushdown"  # Used as name and identifier
DEFAULT_FFS_VALUES = {PUSHDOWN_FF: FeatureFlag(name=PUSHDOWN_FF, identifier=PUSHDOWN_FF, value=False)}  # Const
