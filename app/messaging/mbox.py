from enum import auto, IntEnum


class Mbox(IntEnum):
    UPDATER = auto()
    PUBSUB = auto()
