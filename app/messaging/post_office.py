from typing import Dict, Union

from asyncio import Queue
from asyncio_multisubscriber_queue import MultisubscriberQueue
from messaging.data import Data
from messaging.mbox import Mbox


QUEUES_MAPPING = {Mbox.UPDATER: MultisubscriberQueue, Mbox.PUBSUB: Queue}


class PostOffice:
    queue_map: Dict[Mbox, Union[MultisubscriberQueue, Queue]] = {}

    @classmethod
    def _get_queue(cls, mbox_name: Mbox):
        if mbox_name not in cls.queue_map:
            cls.queue_map[mbox_name] = QUEUES_MAPPING[mbox_name]()
        return cls.queue_map[mbox_name]

    @classmethod
    async def send_msg(cls, mbox_name: Mbox, msg: Data):
        if isinstance(msg, Data):
            await cls._get_queue(mbox_name).put(msg)
        else:
            raise TypeError('invalid PostOffice msg type')

    @classmethod
    async def get_msg(cls, mbox_name: Mbox):
        queue = cls._get_queue(mbox_name)
        if type(queue) is MultisubscriberQueue:
            async for data in queue.subscribe():
                yield data

        elif type(queue) is Queue:
            while True:
                yield await queue.get()

        else:
            raise TypeError(f"Invalid Queue type: {type(queue)}")
