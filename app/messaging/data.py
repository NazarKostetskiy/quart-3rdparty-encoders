from dataclasses import dataclass
from typing import Any


@dataclass
class Data:
    id: Any
    data: Any

    def __str__(self):
        return f"<{self.__class__.__name__} object: id = {self.id}>"
