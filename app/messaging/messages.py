from enum import Enum, auto


class MsgID(Enum):
    MSG_PUB_EVENT = auto()
    MSG_FEATURE_FLAG_UPDATE_EVENT = auto()
