import logging

import socketio
from socketio import AsyncRedisManager

from app import quart_app
from utils.quart_blueprint import register_blueprints
from utils.utils import generate_pydantic_response

logger = logging.getLogger(__name__)

logging.basicConfig(level=logging.DEBUG)

# Used only in socketio
queue_manager = AsyncRedisManager('redis://', logger=logger)


sio = socketio.AsyncServer(client_manager=queue_manager,
                           async_mode='asgi')

socketio_app = socketio.ASGIApp(sio, quart_app)
register_blueprints(quart_app)


@quart_app.route("/data")
# @validate_response(ResponseModel)
# Validation works so slow on example with large json, about 4x of initial response time.
# Maybe this is pydantic issue, but maybe this is quart-schemas
# Seems like it call model.dict() and then quart trying to encode this again.
# Need to investigate it, but anyway it's better for now don't validate response on large data
async def get_data():
    # >3 sec difference between orjson and default. Also using 3rd party json.loads function can decrease response time
    # return await get_json_data()

    # ~1.8 sec difference between orjson and default
    return generate_pydantic_response()
