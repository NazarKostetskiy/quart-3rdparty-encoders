from dependency_injector import containers, providers

from harness_feature_flag.aio.async_client import AsyncHarnessFeatureFlagsClient
from harness_feature_flag.aio.provider import initialize_and_run_feature_flags_manager
from pubsub.manager import PubSubRedisManager
from pubsub.provider import init_pubsub_manager


class AppContainer(containers.DeclarativeContainer):
    ffs_client: AsyncHarnessFeatureFlagsClient = providers.Resource(initialize_and_run_feature_flags_manager)
    pubsub_manager: PubSubRedisManager = providers.Resource(init_pubsub_manager)
