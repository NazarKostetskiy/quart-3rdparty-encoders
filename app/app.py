from quart import Quart
from quart_schema import QuartSchema

from container import AppContainer
from utils.utils import json_encoder

quart_app = Quart(__name__)
QuartSchema(quart_app)

# Change it for test
quart_app.json_encoder = json_encoder

container = AppContainer()


@quart_app.before_serving
async def startup():
    quart_app.container = container
    container.wire(packages=["api.sse"])
    await container.init_resources()


@quart_app.after_serving
async def shutdown():
    await container.shutdown_resources()
