import asyncio
import logging
import pickle
from messaging.mbox import Mbox
from messaging.post_office import PostOffice

try:
    import aioredis
except ImportError:
    aioredis = None

default_logger = logging.getLogger(__name__)


class PubSubRedisManager:  # pragma: no cover
    """Pubsub redis manager"""

    name = 'aioredis'

    def __init__(self, url='redis://localhost:6379/0', channel='pubsub',
                 write_only=False, logger=None, redis_options=None):
        if aioredis is None:
            raise RuntimeError('Redis package is not installed '
                               '(Run "pip install aioredis" in your '
                               'virtualenv).')
        if not hasattr(aioredis.Redis, 'from_url'):
            raise RuntimeError('Version 2 of aioredis package is required.')
        self.redis_url = url
        self.redis_options = redis_options or {}
        self._redis_connect()
        self.channel = channel
        self.write_only = write_only
        self.logger = logger

    def _redis_connect(self):
        self.redis = aioredis.Redis.from_url(self.redis_url,
                                             **self.redis_options)
        self.pubsub = self.redis.pubsub(ignore_subscribe_messages=True)

    async def _publish(self, data):
        retry = True
        while True:
            print("Publishing message...")
            try:
                if not retry:
                    self._redis_connect()
                return await self.redis.publish(
                    self.channel, pickle.dumps(data))
            except aioredis.exceptions.RedisError:
                if retry:
                    self._get_logger().error('Cannot publish to redis... '
                                             'retrying')
                    retry = False
                else:
                    self._get_logger().error('Cannot publish to redis... '
                                             'giving up')
                    break

    async def _redis_listen_with_retries(self):
        retry_sleep = 1
        connect = False
        while True:
            try:
                if connect:
                    self._redis_connect()
                    await self.pubsub.subscribe(self.channel)
                    retry_sleep = 1
                async for message in self.pubsub.listen():
                    print("Got redis message")
                    yield message

            except aioredis.exceptions.RedisError:
                self._get_logger().error('Cannot receive from redis... '
                                         'retrying in '
                                         '{} secs'.format(retry_sleep))
                connect = True
                await asyncio.sleep(retry_sleep)
                retry_sleep *= 2
                if retry_sleep > 60:
                    retry_sleep = 60

    async def listen_subscribe_events(self):
        await self.pubsub.subscribe(self.channel)
        async for message in self._redis_listen_with_retries():
            print(f"Found message in redis: {message}")
            await PostOffice.send_msg(Mbox.UPDATER, pickle.loads(message["data"]))
        await self.pubsub.unsubscribe(self.channel)

    async def listen_publish_events(self):
        async for message in PostOffice.get_msg(Mbox.PUBSUB):
            await self._publish(message)

    def _get_logger(self):
        """Get the appropriate logger
        Prevents uninitialized servers in write-only mode from failing.
        """
        if self.logger:
            return self.logger
        return default_logger

    # Check for dependency injection testing. TODO: Remove
    async def test(self):
        print("test")
        await asyncio.sleep(1)
        print("end test")

    async def close(self):
        await self.redis.close()
        await self.pubsub.close()
