import asyncio
from pubsub.manager import PubSubRedisManager


async def init_pubsub_manager():
    pubsub_manager = PubSubRedisManager()

    asyncio.create_task(pubsub_manager.listen_subscribe_events())
    asyncio.create_task(pubsub_manager.listen_publish_events())

    yield pubsub_manager

    await pubsub_manager.close()
