from quart.json import JSONEncoder
import ujson
import orjson


class UltraJsonEncoder(JSONEncoder):
    def encode(self, o):
        return ujson.dumps(o)


class OrJsonEncoder(JSONEncoder):
    def encode(self, o):
        return orjson.dumps(o)
