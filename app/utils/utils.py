import json
from dataclasses import dataclass
from json import JSONEncoder
from typing import Any, Optional

import aiofiles
import ujson
from orjson import orjson
from pydantic import BaseModel

from .encoders import OrJsonEncoder, UltraJsonEncoder

json_encoder = OrJsonEncoder


async def get_json_data(file='test_data.json') -> dict:
    async with aiofiles.open(file, mode='r') as f:
        contents = await f.read()
    return json.loads(contents)  # json_loads(contents)


def generate_pydantic_response() -> str:
    return ResponseModel(**{"type": "type", "features": [{str(value): value} for value in range(5555555)]}).json()


def _orjson_dumps(val, *, default):
    return orjson.dumps(val, default=default).decode()


if json_encoder is OrJsonEncoder:
    json_loads = orjson.loads
    json_dumps = _orjson_dumps
elif json_encoder is UltraJsonEncoder:
    json_loads = ujson.loads
    json_dumps = ujson.dumps
else:
    json_loads = json.loads
    json_dumps = json.dumps


class ResponseModel(BaseModel):
    type: str
    features: list

    class Config:
        json_loads = json_loads
        json_dumps = json_dumps


@dataclass
class ResponseDataclass:
    type: str
    features: list


def to_camel(string: str) -> str:
    string_split = string.split('_')
    return string_split[0]+''.join(word.capitalize() for word in string_split[1:])


@dataclass
class Headers:
    x_token: Any
    authorization: Optional[str] = None
