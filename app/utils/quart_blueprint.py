from quart import Quart

from api.rest.views import default_blueprint
from api.sse.views import sse_blueprint


def register_blueprints(quart_app: Quart):
    import api.socketio.event_handlers  # noqa
    quart_app.register_blueprint(default_blueprint)
    quart_app.register_blueprint(sse_blueprint)
