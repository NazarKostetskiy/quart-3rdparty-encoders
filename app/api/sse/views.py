import random
from asyncio import CancelledError
from dataclasses import dataclass
from uuid import uuid4

import orjson
from dependency_injector.wiring import inject, Provide
from quart import Blueprint, make_response
from aiocache import Cache
from api.sse.message import ServerSentEvent
from container import AppContainer
from messaging.mbox import Mbox
from messaging.messages import MsgID
from messaging.post_office import PostOffice
from pubsub.manager import PubSubRedisManager

sse_blueprint = Blueprint('pubsub', __name__)

xids_tenants = [('adcipm9', 'studio'), ('adcipm9', 'moffice'), ('adcicl', 'studio'), ('adcicl', 'moffice')]
clients = set()


@dataclass(frozen=True)
class Client:
    id: str
    xid: str
    tenant_id: str


@dataclass
class PubMessage:
    data: dict


def add_new_client() -> Client:
    client = Client(str(uuid4()), *random.choice(xids_tenants))
    clients.add(client)
    return client


@sse_blueprint.route("/sse", strict_slashes=False)
@inject
async def sse(cache: PubSubRedisManager = Provide[AppContainer.pubsub_manager]):
    client = add_new_client()

    await cache.test()  # Example of using dependency injection

    async def listen_updater_events():
        async for msg in PostOffice.get_msg(Mbox.UPDATER):
            print(f"MSG in UPDATER QUEUE: {msg}")
            if msg.id == MsgID.MSG_FEATURE_FLAG_UPDATE_EVENT:
                data = orjson.dumps(msg.data).decode()
                yield ServerSentEvent(data=data).encode()

    try:
        response = await make_response(listen_updater_events(), {
            'Content-Type': 'text/event-stream',
            'Cache-Control': 'no-cache',
            'Transfer-Encoding': 'chunked',
        })

        response.timeout = None
        return response

    # Remove client on disconnect
    except CancelledError:
        clients.remove(client)
