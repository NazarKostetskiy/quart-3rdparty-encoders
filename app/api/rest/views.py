from quart import Blueprint, jsonify

from harness_feature_flag.aio.provider import feature_flags

default_blueprint = Blueprint('default', __name__)


@default_blueprint.route("/")
async def index():
    return "Ok", 200


@default_blueprint.route("/feature-flags")
async def get_feature_flags():
    return jsonify(feature_flags), 200
