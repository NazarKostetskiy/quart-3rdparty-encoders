from api.socketio.events import SocketioEventList
from main import sio


@sio.event
async def connect(sid, environ, auth):
    print('connect ', sid)


@sio.on(SocketioEventList.EVT_TEST_REQ)
async def on_test_handler(sid, msg):
    print(f"sid: {sid}, msg: {msg}")
    await sio.emit(SocketioEventList.EVT_TEST_RESP, "ok")


@sio.event
async def disconnect(sid):
    print('disconnect ', sid)
