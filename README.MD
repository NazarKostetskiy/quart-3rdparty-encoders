## How to run and test
1. Install pipenv on your machine
2. Run `pipenv install` and `pipenv install --dev`


## Run backend uvicorn main:app and then:
1. Change encoder in main.py
2. Run tests
3. Compare using following command: `pytest --durations=0 -vv performance_tests/`


## Results:

orjson is better solution than ujson:
1) it is faster
2) it has support for many different python types
3) better community and less issues on github


Results with default encoder
8.94s
8.89s
8.87s
8.66s
8.65s
8.63s
8.56s
8.45s
8.31s
8.29s


using pydantic response model for validation:
25.73s
25.27s
24.97s
24.55s
24.37s
24.36s
24.33s
24.27s
24.07s
23.93s


Results for ujson

5.79s
5.89s
5.79s
5.88s
5.58s
5.88s
5.90s
5.52s
5.67s
5.75s

Results for orjson

4.91s
4.70s
4.65s
4.57s
4.49s
4.41s
4.36s
4.31s
4.25s
4.19s


Using pydantic response model for validation:
23.51s
21.15s
21.14s
20.75s
20.72s
20.67s
20.62s
20.59s
20.45s
20.12s




Ultrajson has bad default encoder without support of decimal, uuid and other types.

Need to investigate our response validation to understand how we can improve performance there.

For now the best way to improve performance is to not validate response and use orjson library.

Tested on Lubuntu 20.04 LTS

________________________

# Python Socketio

Run server: `pipenv run gunicorn main:socketio_app --workers 2 --worker-class uvicorn.workers.UvicornWorker --log-level debug`

And then you can use socketio_client.py to test it.
